import os
import random

from PIL import Image

__author__ = 'primankaden'


def write(data, file_name, new_file_name):
    im = Image.open(file_name)
    im.convert("RGBA")
    pixels = im.load()
    for i in range(len(data)):
        for j in range(4):
            a = (ord(data[i]) >> (7 - 2 * j)) % 2
            b = (ord(data[i]) >> (7 - 2 * j - 1)) % 2
            (red, green, blue) = im.getpixel((4 * i + j, 0))[:3]
            c0 = red % 2
            c1 = green % 2
            c2 = blue % 2
            c = (c0 + c1) % 2
            d = (c1 + c2) % 2
            if a == c and b != d:
                blue ^= 1
            elif a != c and b == d:
                red ^= 1
            elif a != c and b != d:
                green ^= 1
            pixels[4 * i + j, 0] = (red, green, blue)
    im.save(new_file_name)


def read(file_name):
    im = Image.open(file_name)
    im.convert("RGBA")
    all_data = ''
    data = ""
    bi = 0
    for i in range(im.size[0]):
        bi += 2
        (r, g, b) = im.getpixel((i, 0))[:3]
        c0 = r % 2
        c1 = g % 2
        c2 = b % 2
        a = (c0 + c1) % 2
        b = (c1 + c2) % 2
        data += str(a)
        data += str(b)
        if (bi == 8):
            all_data += chr(int(data, 2))
            data = ""
            bi = 0
            if all_data.find('=') != -1:
                break
    return all_data

def test():
    write("12345678901234567890123=", "img3.png", "new.png")
    print (read("new.png"))

if __name__ == '__main__':
    test()
